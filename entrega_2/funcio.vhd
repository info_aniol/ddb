ENTITY testbench IS
END testbench;

ARCHITECTURE test OF testbench IS

COMPONENT funcio_2
port(a, b, c, d: IN BIT; f: OUT BIT);
END COMPONENT;




SIGNAL ent0, ent1, ent2, ent3, sort_logica, sort_logicaretard, sort_estructural, sort_estructuralretard, sort_ifthen: BIT;

FOR DUT0: funcio_2 USE ENTITY WORK.funcio_2(logica);
FOR DUT1: funcio_2 USE ENTITY WORK.funcio_2(logicaretard);
FOR DUT2: funcio_2 USE ENTITY WORK.funcio_2(estructural);
FOR DUT3: funcio_2 USE ENTITY WORK.funcio_2(estructuralretard);
FOR DUT4: funcio_2 USE ENTITY WORK.funcio_2(ifthen);

BEGIN

DUT0: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_logica);
DUT1: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_logicaretard);
DUT2: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_estructural);
DUT3: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_estructuralretard);
DUT4: funcio_2 PORT MAP (ent3, ent2, ent1, ent0, sort_ifthen);

PROCESS(ent3, ent2, ent1, ent0)
BEGIN
ent0 <= NOT ent0 AFTER 5 ns;
ent1 <= NOT ent1 AFTER 10 ns;
ent2 <= NOT ent2 AFTER 20 ns;
ent3 <= NOT ent3 AFTER 40 ns;
END PROCESS;
END TEST;