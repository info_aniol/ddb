ENTITY testbench IS
END testbench;

ARCHITECTURE test OF testbench IS

COMPONENT inversor
port(a: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT and2
PORT(a, b: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT and3
PORT(a, b, c: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT and4
PORT(a, b, c, d: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or2
PORT(a, b: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or3
PORT(a, b, c: IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT or4
PORT(a, b, c, d: IN BIT; z: OUT BIT);
END COMPONENT;



SIGNAL ent1, ent2, ent3, ent4, sort_inversor_logica, sort_and2_logica, sort_and3_logica, sort_and4_logica, sort_or2_logica, sort_or3_logica, sort_or4_logica: BIT;

FOR DUT0: inversor USE ENTITY WORK.inversor(logica);
FOR DUT1: and2 USE ENTITY WORK.and2(logica);
FOR DUT2: and3 USE ENTITY WORK.and3(logica);
FOR DUT3: and4 USE ENTITY WORK.and4(logica);
FOR DUT4: or2 USE ENTITY WORK.or2(logica);
FOR DUT5: or3 USE ENTITY WORK.or3(logica);
FOR DUT6: or4 USE ENTITY WORK.or4(logica);

BEGIN
DUT0: inversor PORT MAP (ent1, sort_inversor_logica);
DUT1: and2 PORT MAP (ent1, ent2, sort_and2_logica);
DUT2: and3 PORT MAP (ent1, ent2, ent3, sort_and3_logica);
DUT3: and4 PORT MAP (ent1, ent2, ent3, ent4, sort_and4_logica);
DUT4: or2 PORT MAP (ent1, ent2, sort_or2_logica);
DUT5: or3 PORT MAP (ent1, ent2, ent3, sort_or3_logica);
DUT6: or4 PORT MAP (ent1, ent2, ent3, ent4, sort_or4_logica);

PROCESS
BEGIN
ent1 <= '0';
ent2 <= '0';
ent3 <= '0';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '0';
ent3 <= '0';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '1';
ent3 <= '0';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '1';
ent3 <= '0';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '0';
ent3 <= '1';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '0';
ent3 <= '1';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '1';
ent3 <= '1';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '1';
ent3 <= '1';
ent4 <= '0';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '0';
ent3 <= '0';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '0';
ent3 <= '0';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '1';
ent3 <= '0';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '1';
ent3 <= '0';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '0';
ent3 <= '1';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '0';
ent3 <= '1';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '0';
ent2 <= '1';
ent3 <= '1';
ent4 <= '1';
WAIT FOR 50 ns;
ent1 <= '1';
ent2 <= '1';
ent3 <= '1';
ent4 <= '1';
WAIT FOR 50 ns;
END PROCESS;
END TEST;