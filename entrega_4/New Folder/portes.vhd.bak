-- INVERSOR  -----------------------------------------
ENTITY inv IS 
        PORT(a:IN BIT; z:OUT BIT);
END inv;


ARCHITECTURE logica OF inv IS
BEGIN
        z<= NOT a;
END logica;


ARCHITECTURE logica_retard OF inv IS
BEGIN
        z<= NOT a AFTER 3 ns;
END logica_retard;

-- AND2 -----------------------------------------
ENTITY and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END and2;


ARCHITECTURE logica OF and2 IS
BEGIN
        z<= a AND b;
END logica;

ARCHITECTURE logica_retard OF and2 IS
BEGIN
        z<= a AND b  AFTER 3 ns;
END logica_retard;


-- NOR2 -----------------------------------------
ENTITY nor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END nor2;


ARCHITECTURE logica OF nor2 IS
BEGIN
        z<= NOT (a OR b);
END logica;

ARCHITECTURE logica_retard OF nor2 IS
BEGIN
        z<= NOT (a OR b)  AFTER 5 ns;
END logica_retard;

------------------------------------------------------------------------------------
--- D FLIP-FLOP
ENTITY D_Bajada_PreClr IS
PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END D_Bajada_PreClr;

ARCHITECTURE ifthen OF D_Bajada_PreClr IS
SIGNAL qint: BIT;
BEGIN

PROCESS (D,Clk,Pre,Clr)
BEGIN
IF Clr='0' THEN qint<='0' AFTER 3 ns;
     ELSIF Pre='0' THEN qint<='1' AFTER 3 ns;
     ELSIF Clk'EVENT AND Clk='0' THEN
     qint <= D AFTER 6 ns;

END IF;
END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;

----------------------------------------------------------------------------------
--- D LATCH

ENTITY D_Latch_PreClr IS
PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END D_Latch_PreClr;

ARCHITECTURE ifthen OF D_Latch_PreClr IS
SIGNAL qint: BIT;
BEGIN

PROCESS (D,Clk,Pre,Clr)
BEGIN
	IF Clr='0' THEN 
		qint<='0' AFTER 3 ns;
     	ELSIF Pre='0' THEN 
		qint<='1' AFTER 3 ns;
     	ELSIF Clk='1' THEN
	     qint <= D AFTER 6 ns;

END IF;
END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;

----------------------------------------------------------------------------------
--- JK FLIP-FLOP

ENTITY JK_Down_PreClr IS
PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END JK_Down_PreClr;

ARCHITECTURE ifthen OF JK_Down_PreClr IS
SIGNAL qint: BIT;
BEGIN
PROCESS (J,K,Clk,Pre,Clr)
BEGIN
IF Clr='0' THEN qint<='0' AFTER 3 ns;
ELSE
	IF Pre='0' THEN qint<='1' AFTER 3 ns;
	ELSE
        	IF Clk'EVENT AND Clk='0' THEN
			IF J='0' AND K='0' THEN qint<=qint AFTER 6 ns;
        	        ELSIF J='0' AND K='1' THEN qint<='0' AFTER 6 ns;
        	        ELSIF J='1' AND K='0' THEN qint<='1' AFTER 6 ns;
        	        ELSIF J='1' AND K='1' THEN qint<= NOT qint AFTER 6 ns;
        	        END IF;
		
        	END IF;
        END IF;
END IF;

END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;

----------------------------------------------------------------------------------
--- JK LATCH

ENTITY JK_Latch_PreClr IS
PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END JK_Latch_PreClr;

ARCHITECTURE ifthen OF JK_Latch_PreClr IS
SIGNAL qint: BIT;
BEGIN
PROCESS (J,K,Clk,Pre,Clr)
BEGIN
IF Clr='0' THEN qint<='0' AFTER 3 ns;
ELSE
IF Pre='0' THEN qint<='1' AFTER 3 ns;
ELSE
                IF Clk='1' THEN
                        IF J='0' AND K='0' THEN qint<=qint AFTER 6 ns;
                        ELSIF J='0' AND K='1' THEN qint<='0' AFTER 6 ns;
                        ELSIF J='1' AND K='0' THEN qint<='1' AFTER 6 ns;
                        ELSIF J='1' AND K='1' THEN qint<= NOT qint AFTER 6 ns;
                        END IF;

                END IF;
        END IF;
END IF;

END PROCESS;
Q<=qint; NO_Q<=NOT qint;
END ifthen;



----------------------------------------------------------------------------------------------
-- combinacional
ENTITY comb IS
	PORT(x, Ck, preset, clear: IN BIT; z: OUT BIT);
END comb;

ARCHITECTURE estructural OF comb IS
COMPONENT inv IS
        PORT(a:IN BIT; z:OUT BIT);
END COMPONENT;

COMPONENT and2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT nor2 IS
        PORT(a,b:IN BIT; z: OUT BIT);
END COMPONENT;

COMPONENT D_Latch_PreClr IS
	PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;

COMPONENT JK_Down_PreClr IS
	PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;

FOR DUT1: inv USE ENTITY WORK.inv(logica_retard);
FOR DUT2: D_Latch_PreClr USE ENTITY WORK.D_Latch_PreClr(ifthen);
FOR DUT3: and2 USE ENTITY WORK.and2(logica_retard);
FOR DUT4: nor2 USE ENTITY WORK.nor2(logica_retard);
FOR DUT5: JK_Down_PreClr USE ENTITY WORK.JK_Down_PreClr(ifthen);

SIGNAL NO_x, Q1, NO_Q1, j, k, NO_z: BIT;

BEGIN
DUT1: inv PORT MAP(x, NO_x);
DUT2: D_Latch_PreClr PORT MAP(x, Ck, preset, clear, Q1, NO_Q1);
DUT3: and2 PORT MAP(x, NO_Q1, j);
DUT4: nor2 PORT MAP(Q1, NO_x, k);
DUT5: JK_Down_PreClr PORT MAP(j, k, Ck, preset, clear, z, NO_z);

END estructural;

ENTITY testbench IS
END testbench;

ARCHITECTURE test OF testbench IS
	COMPONENT comb IS
		PORT (x, Ck, preset, clear :IN BIT; z:OUT BIT);
	END COMPONENT;
SIGNAL ent1, clock, pre, clr, sort: BIT;
FOR DUT1: comb USE ENTITY WORK.comb(estructural);
BEGIN
DUT1: comb PORT MAP (ent1, clock, pre, clr, sort);
clr <= '1';
pre <= '1';
clock <= NOT clock AFTER 50 ns;
ent1 <='0', '1' AFTER 75 ns, '0' AFTER 175 ns, '1' AFTER 255 ns, '0' AFTER 265 ns, '1' AFTER 275 ns, '0' AFTER 290 ns, '1' AFTER 325 ns, '0' AFTER 375 ns;
END test;

ENTITY banco_pruebas IS
END banco_pruebas;

ARCHITECTURE test OF banco_pruebas IS
COMPONENT mi_D_Bajada_PreClr IS
PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;
COMPONENT mi_JK_Latch_PreClr IS
PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;
COMPONENT mi_D_Latch_PreClr IS
PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;
COMPONENT mi_JK_Down_PreClr IS
PORT(J,K,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END COMPONENT;
SIGNAL ent1,ent2,clock,preset,clear,Dsort_Q,Dsort_noQ,JKsort_Q,JKsort_noQ,DLsort_Q,DLsort_noQ,JKDsort_Q,JKDsort_noQ: BIT;
FOR DUT1: mi_D_Bajada_PreClr USE ENTITY WORK.D_Bajada_PreClr(ifthen);
FOR DUT2: mi_JK_Latch_PreClr USE ENTITY WORK.JK_Latch_PreClr(ifthen);
FOR DUT3: mi_D_Latch_PreClr USE ENTITY WORK.D_Latch_PreClr(ifthen);
FOR DUT4: mi_JK_Down_PreClr USE ENTITY WORK.JK_Down_PreClr(ifthen);
BEGIN
DUT1: mi_D_Bajada_PreClr PORT MAP (ent1,clock,preset,clear,Dsort_Q,Dsort_noQ);
DUT2: mi_JK_Latch_PreClr PORT MAP (ent1,ent2,clock,preset,clear,JKsort_Q,JKsort_noQ);
DUT3: mi_D_Latch_PreClr PORT MAP (ent1,clock,preset,clear,DLsort_Q,DLsort_noQ);
DUT4: mi_JK_Down_PreClr PORT MAP (ent1,ent2,clock,preset,clear,JKDsort_Q,JKDsort_noQ);
ent1 <= NOT ent1 AFTER 800 ns;

ent2 <= NOT ent2 AFTER 400 ns;
clock <= NOT clock AFTER 500 ns;
preset <= '0', '1' AFTER 600 ns;
clear <= '1','0' AFTER 200 ns, '1' AFTER 400 ns;
-- simular hasta 15000 ns
END test;
