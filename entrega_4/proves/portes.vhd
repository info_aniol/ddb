
ENTITY Latch_D IS
PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
END Latch_D;

ARCHITECTURE ifthen OF Latch_D IS
SIGNAL qint: BIT;
BEGIN

PROCESS (D,Clk,Pre,Clr)
BEGIN
	IF Clr='0' THEN 
		qint<='0' AFTER 3 ns;
     	ELSIF Pre='0' THEN 
		qint<='1' AFTER 3 ns;
     	ELSIF Clk='1' THEN
	     qint <= D AFTER 6 ns;

END IF;
END PROCESS;
Q<=qint; NO_Q<=NOT qint;
-- Aqu� es on es fa l?assignaci� de les variables
-- internes a les variables de sortida
END ifthen;

ENTITY testbench IS
END testbench;

ARCHITECTURE test OF testbench IS
	COMPONENT Latch_D IS
		PORT(D,Clk,Pre,Clr: IN BIT; Q,NO_Q: OUT BIT);
	END COMPONENT;

SIGNAL entrada ,clock, preset, clear, sortida, no_sort:BIT;

FOR DUT1: Latch_D USE ENTITY WORK.Latch_D(ifthen);
BEGIN
DUT1: Latch_D PORT MAP(entrada, clock, preset, clear, sortida, no_sort);
clock <= NOT clock AFTER 50 ns;
clear <= '1';
preset <= '1';
entrada <= NOT entrada AFTER 32 ns;

END test;
